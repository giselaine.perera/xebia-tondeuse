package com.exercice.xebiatondeuse;

import com.exercice.xebiatondeuse.service.ControlerTondeuseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XebiaTondeuseApplication implements ApplicationRunner {

    @Autowired
    ControlerTondeuseService controlerTondeuseService;

    private static final Logger LOGGER = LoggerFactory.getLogger(XebiaTondeuseApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(XebiaTondeuseApplication.class, args);
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        if(args.getSourceArgs().length != 1){
            LOGGER.error("Nombre d'arguments incorrect, veuillez indiquer uniquement le chemin du fichier à traiter");
            return;
        }
        System.out.println("Le fichier à traiter se situe dans : " + args.getSourceArgs()[0]);

        controlerTondeuseService.programmerTondeuses(args.getSourceArgs()[0]);
    }
}
