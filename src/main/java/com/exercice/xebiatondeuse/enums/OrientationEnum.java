package com.exercice.xebiatondeuse.enums;

public enum OrientationEnum {
    N,
    E,
    S,
    W;

    /**
     * Renvoi l'orientation pour une rotation à gauche
     * @param orientationEnum orientation de départ
     * @return orientation finale
     */
    public static OrientationEnum rotationGauche(OrientationEnum orientationEnum){
        OrientationEnum orientationGauche;
        switch (orientationEnum){
            case N : orientationGauche=OrientationEnum.W; break;
            case E : orientationGauche=OrientationEnum.N; break;
            case S : orientationGauche=OrientationEnum.E; break;
            case W : orientationGauche=OrientationEnum.S; break;
            default:
                throw new IllegalStateException("Unexpected value: " + orientationEnum);
        }

        return orientationGauche;
    }

    /**
     * Renvoi l'orientation pour une rotation à droite
     * @param orientationEnum orientation de départ
     * @return orientation finale
     */
    public static OrientationEnum rotationDroite(OrientationEnum orientationEnum){
        OrientationEnum orientationDroite;
        switch (orientationEnum){
            case N : orientationDroite=OrientationEnum.E; break;
            case E : orientationDroite=OrientationEnum.S; break;
            case S : orientationDroite=OrientationEnum.W; break;
            case W : orientationDroite=OrientationEnum.N; break;
            default:
                throw new IllegalStateException("Unexpected value: " + orientationEnum);
        }

        return orientationDroite;
    }
}
