package com.exercice.xebiatondeuse.exception;

/**
 * Exception levée lors du traitement d'exception
 */
public class InstructionException extends Exception {
    public InstructionException(String s) {
        super(s);
    }
}
