package com.exercice.xebiatondeuse.exception;

/**
 * Exception levée lors de l'initialisation de la pelouse
 */
public class PelouseException extends Exception{
    public PelouseException(String s) {
        super(s);
    }
}
