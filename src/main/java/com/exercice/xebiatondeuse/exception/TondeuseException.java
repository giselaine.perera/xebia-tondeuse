package com.exercice.xebiatondeuse.exception;

/**
 * Exception levée lors de l'initialisation de la tondeuse
 */
public class TondeuseException extends  Exception{
    public TondeuseException(String s) {
        super(s);
    }
}
