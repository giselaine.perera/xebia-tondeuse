package com.exercice.xebiatondeuse.model;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;

@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Coordonnees {
    Integer x;
    Integer y;

    @Override
    public String toString() {
        return x + " " + y;
    }
}
