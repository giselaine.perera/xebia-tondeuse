package com.exercice.xebiatondeuse.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Pelouse {
    Coordonnees boutTerrain;
}
