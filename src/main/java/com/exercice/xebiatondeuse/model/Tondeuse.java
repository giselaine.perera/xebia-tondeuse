package com.exercice.xebiatondeuse.model;

import com.exercice.xebiatondeuse.enums.OrientationEnum;
import lombok.*;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Tondeuse {
    Coordonnees coordonnees;
    OrientationEnum orientation;

    @Override
    public String toString(){
        return coordonnees.toString() + " " + orientation.name();
    }
}
