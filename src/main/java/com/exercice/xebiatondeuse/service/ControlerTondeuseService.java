package com.exercice.xebiatondeuse.service;

import com.exercice.xebiatondeuse.enums.DeplacementEnum;
import com.exercice.xebiatondeuse.enums.OrientationEnum;
import com.exercice.xebiatondeuse.exception.InstructionException;
import com.exercice.xebiatondeuse.exception.PelouseException;
import com.exercice.xebiatondeuse.exception.TondeuseException;
import com.exercice.xebiatondeuse.model.Coordonnees;
import com.exercice.xebiatondeuse.model.Pelouse;
import com.exercice.xebiatondeuse.model.Tondeuse;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.Set;

/**
 * Service qui initialise le terrain et controle des tondeuses
 */
@Service
public class ControlerTondeuseService {

    @Autowired
    TondeuseService tondeuseService;

    public static final String SPACE_CHARACTER = " ";

    /**
     * Méthode permettant de traiter les instructions contenues dans le fichier
     * @param filename
     */
    public void programmerTondeuses(String filename) throws IOException, InstructionException, PelouseException, TondeuseException {
        File fichierEntree = new File(filename);

        if(!fichierEntree.exists()){
            throw new FileNotFoundException("Le fichier passé en paramètre n'existe pas");
        }

        String fichierSortie = fichierEntree.getPath().replace(".txt", "") + "_position_finale.txt";

        try (FileWriter writer = new FileWriter(fichierSortie); Scanner reader = new Scanner(fichierEntree)){
            // Traitement de la pelouse
            Pelouse pelouse;
            try {
                String[] lignePelouse = reader.nextLine().split(SPACE_CHARACTER);
                Coordonnees coordonneesPelouse = new Coordonnees(Integer.valueOf(lignePelouse[0]), Integer.valueOf(lignePelouse[1]));
                pelouse = new Pelouse(coordonneesPelouse);

            } catch (NumberFormatException | IndexOutOfBoundsException e){
                throw new PelouseException("Erreur lors de l'initialisation de la pelouse");
            }

            while (reader.hasNext()) {
                // Traitement de la tondeuse
                Tondeuse tondeuse;
                try {
                    String[] ligneTondeuse = reader.nextLine().split(SPACE_CHARACTER);

                    Coordonnees coordonneesTondeuses = new Coordonnees(Integer.valueOf(ligneTondeuse[0]), Integer.valueOf(ligneTondeuse[1]));
                    tondeuse = new Tondeuse(coordonneesTondeuses, OrientationEnum.valueOf(ligneTondeuse[2]));
                } catch (IllegalArgumentException | IndexOutOfBoundsException e){
                    throw new TondeuseException("Erreur lors de l'initialisation de la tondeuse");
                }

                String instructions = reader.nextLine();

                // traitement des instructions
                try {
                    CollectionUtils.arrayToList(instructions.split("")).stream()
                            .forEach(instruction -> {
                                DeplacementEnum deplacement = DeplacementEnum.valueOf((String) instruction);

                                switch (deplacement) {
                                    case G:
                                        tondeuseService.rotationGauche(tondeuse);
                                        break;
                                    case D:
                                        tondeuseService.rotationDroite(tondeuse);
                                        break;
                                    case A:
                                        tondeuseService.deplacerTondeuse(tondeuse, pelouse);
                                        break;
                                }
                            });
                } catch(IllegalArgumentException e){
                    throw new InstructionException("L'instruction ne correspond pas à ce qui est attendu : A, G, D");
                }

                writer.append(tondeuse.toString() + Strings.LINE_SEPARATOR);
            }
        }
    }
}
