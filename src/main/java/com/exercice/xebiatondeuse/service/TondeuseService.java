package com.exercice.xebiatondeuse.service;

import com.exercice.xebiatondeuse.enums.OrientationEnum;
import com.exercice.xebiatondeuse.model.Pelouse;
import com.exercice.xebiatondeuse.model.Tondeuse;
import org.springframework.stereotype.Service;

/**
 * Service de gestion de la tondeuse
 */
@Service
public class TondeuseService {

    /**
     * Méthode permettant d'avancer la tondeuse dans son orientation sur la pelouse
     * @param tondeuse
     * @param pelouse
     */
    public void deplacerTondeuse(Tondeuse tondeuse, Pelouse pelouse){
        Integer x = tondeuse.getCoordonnees().getX();
        Integer y = tondeuse.getCoordonnees().getY();
        switch (tondeuse.getOrientation()){
            case N: if (y+1 <= pelouse.getBoutTerrain().getY()) y++;break;
            case E: if (x+1 <= pelouse.getBoutTerrain().getY()) x++;break;
            case S: if (y-1 >= 0) y--;break;
            case W: if (x-1 >= 0) x--;break;
        }

        tondeuse.getCoordonnees().setX(x);
        tondeuse.getCoordonnees().setY(y);
    }

    /**
     * Méthode permettant de faire une rotation à gauche de 90°
     * @param tondeuse
     */
    public void rotationGauche(Tondeuse tondeuse){
        tondeuse.setOrientation(OrientationEnum.rotationGauche(tondeuse.getOrientation()));
    }

    /**
     * Méthode permettant de faire une rotation à droite de 90°
     * @param tondeuse
     */
    public void rotationDroite(Tondeuse tondeuse){
        tondeuse.setOrientation(OrientationEnum.rotationDroite(tondeuse.getOrientation()));
    }
}
