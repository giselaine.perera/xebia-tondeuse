package com.exercice.xebiatondeuse.service;

import com.exercice.xebiatondeuse.XebiaTondeuseApplication;
import com.exercice.xebiatondeuse.exception.InstructionException;
import com.exercice.xebiatondeuse.exception.PelouseException;
import com.exercice.xebiatondeuse.exception.TondeuseException;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.util.Strings;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.ActiveProfiles;

import java.io.File;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
class ControlerTondeuseServiceTest {
    @Autowired
    ControlerTondeuseService controlerTondeuseService;

    @Test
    void programmerTondeuseTest() throws IOException, InstructionException, TondeuseException, PelouseException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("direction_tondeuse.txt").getFile());
        controlerTondeuseService.programmerTondeuses(file.getAbsolutePath());

        File fichierSortie = new File(classLoader.getResource("direction_tondeuse_position_finale.txt").getFile());

        Assertions.assertTrue(fichierSortie.exists());
        String expectedSortie = FileUtils.readFileToString(fichierSortie, "UTF-8");
        Assertions.assertEquals("1 3 N" + Strings.LINE_SEPARATOR, expectedSortie);
    }

    @Test
    void programmerTondeusesTest() throws IOException, InstructionException, TondeuseException, PelouseException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("direction_tondeuses.txt").getFile());

        controlerTondeuseService.programmerTondeuses(file.getAbsolutePath());

        File fichierSortie = new File(classLoader.getResource("direction_tondeuses_position_finale.txt").getFile());

        Assertions.assertTrue(fichierSortie.exists());
        String expectedSortie = FileUtils.readFileToString(fichierSortie, "UTF-8");
        Assertions.assertEquals("1 3 N" + Strings.LINE_SEPARATOR + "5 1 E" + Strings.LINE_SEPARATOR, expectedSortie);
    }

    @Test
    void programmerTondeuseTondeuseException(){
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("direction_tondeuse_tondeuse_exception.txt").getFile());

        Assertions.assertThrows(TondeuseException.class, () -> {
            controlerTondeuseService.programmerTondeuses(file.getAbsolutePath());
        });
    }

    @Test
    void programmerTondeusePelouseException(){
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("direction_tondeuse_pelouse_exception.txt").getFile());

        Assertions.assertThrows(PelouseException.class, () -> {
            controlerTondeuseService.programmerTondeuses(file.getAbsolutePath());
        });
    }

    @Test
    void programmerTondeuseInstructionException(){
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("direction_tondeuse_instruction_exception.txt").getFile());

        Assertions.assertThrows(InstructionException.class, () -> {
            controlerTondeuseService.programmerTondeuses(file.getAbsolutePath());
        });
    }
}