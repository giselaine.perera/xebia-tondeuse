package com.exercice.xebiatondeuse.service;

import com.exercice.xebiatondeuse.XebiaTondeuseApplication;
import com.exercice.xebiatondeuse.enums.OrientationEnum;
import com.exercice.xebiatondeuse.model.Coordonnees;
import com.exercice.xebiatondeuse.model.Pelouse;
import com.exercice.xebiatondeuse.model.Tondeuse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class TondeuseServiceTest {

    @Autowired
    TondeuseService tondeuseService;

    @Test
    public void deplacerTondeuseNordTest(){
        Tondeuse tondeuse = new Tondeuse(new Coordonnees(5,3), OrientationEnum.N);
        Pelouse pelouse = new Pelouse(new Coordonnees(10,10));

        tondeuseService.deplacerTondeuse(tondeuse, pelouse);

        Assertions.assertAll("Coordonnées de la tondeuse",
                () -> assertEquals(5, tondeuse.getCoordonnees().getX()),
                () -> assertEquals(4, tondeuse.getCoordonnees().getY())
        );
    }

    @Test
    public void deplacerTondeuseEstTest(){
        Tondeuse tondeuse = new Tondeuse(new Coordonnees(5,3), OrientationEnum.E);
        Pelouse pelouse = new Pelouse(new Coordonnees(10,10));

        tondeuseService.deplacerTondeuse(tondeuse, pelouse);

        Assertions.assertAll("Coordonnées de la tondeuse",
                () -> assertEquals(6, tondeuse.getCoordonnees().getX()),
                () -> assertEquals(3, tondeuse.getCoordonnees().getY())
        );
    }

    @Test
    public void deplacerTondeuseSudTest(){
        Tondeuse tondeuse = new Tondeuse(new Coordonnees(5,3), OrientationEnum.S);
        Pelouse pelouse = new Pelouse(new Coordonnees(10,10));

        tondeuseService.deplacerTondeuse(tondeuse, pelouse);

        Assertions.assertAll("Coordonnées de la tondeuse",
                () -> assertEquals(5, tondeuse.getCoordonnees().getX()),
                () -> assertEquals(2, tondeuse.getCoordonnees().getY())
        );
    }

    @Test
    public void deplacerTondeuseOuestTest(){
        Tondeuse tondeuse = new Tondeuse(new Coordonnees(5,3), OrientationEnum.W);
        Pelouse pelouse = new Pelouse(new Coordonnees(10,10));

        tondeuseService.deplacerTondeuse(tondeuse, pelouse);

        Assertions.assertAll("Coordonnées de la tondeuse",
                () -> assertEquals(4, tondeuse.getCoordonnees().getX()),
                () -> assertEquals(3, tondeuse.getCoordonnees().getY())
        );
    }

    @Test
    public void deplacerTondeuseNordBoutTerrainTest(){
        Tondeuse tondeuse = new Tondeuse(new Coordonnees(5,10), OrientationEnum.N);
        Pelouse pelouse = new Pelouse(new Coordonnees(10,10));

        tondeuseService.deplacerTondeuse(tondeuse, pelouse);

        Assertions.assertAll("Coordonnées de la tondeuse",
                () -> assertEquals(5, tondeuse.getCoordonnees().getX()),
                () -> assertEquals(10, tondeuse.getCoordonnees().getY())
        );
    }

    @Test
    public void deplacerTondeuseEstBoutTerrainTest(){
        Tondeuse tondeuse = new Tondeuse(new Coordonnees(10,5), OrientationEnum.E);
        Pelouse pelouse = new Pelouse(new Coordonnees(10,10));

        tondeuseService.deplacerTondeuse(tondeuse, pelouse);

        Assertions.assertAll("Coordonnées de la tondeuse",
                () -> assertEquals(10, tondeuse.getCoordonnees().getX()),
                () -> assertEquals(5, tondeuse.getCoordonnees().getY())
        );
    }

    @Test
    public void deplacerTondeuseSudBoutTerrainTest(){
        Tondeuse tondeuse = new Tondeuse(new Coordonnees(5,0), OrientationEnum.S);
        Pelouse pelouse = new Pelouse(new Coordonnees(10,10));

        tondeuseService.deplacerTondeuse(tondeuse, pelouse);

        Assertions.assertAll("Coordonnées de la tondeuse",
                () -> assertEquals(5, tondeuse.getCoordonnees().getX()),
                () -> assertEquals(0, tondeuse.getCoordonnees().getY())
        );
    }

    @Test
    public void deplacerTondeuseOuestBoutTerrainTest(){
        Tondeuse tondeuse = new Tondeuse(new Coordonnees(0,3), OrientationEnum.W);
        Pelouse pelouse = new Pelouse(new Coordonnees(10,10));

        tondeuseService.deplacerTondeuse(tondeuse, pelouse);

        Assertions.assertAll("Coordonnées de la tondeuse",
                () -> assertEquals(0, tondeuse.getCoordonnees().getX()),
                () -> assertEquals(3, tondeuse.getCoordonnees().getY())
        );
    }


    @Test
    void rotationGaucheTest() {
        Tondeuse tondeuse = new Tondeuse(new Coordonnees(5,3), OrientationEnum.N);

        tondeuseService.rotationGauche(tondeuse);
        assertEquals(OrientationEnum.W, tondeuse.getOrientation());

        tondeuseService.rotationGauche(tondeuse);
        assertEquals(OrientationEnum.S, tondeuse.getOrientation());

        tondeuseService.rotationGauche(tondeuse);
        assertEquals(OrientationEnum.E, tondeuse.getOrientation());

        tondeuseService.rotationGauche(tondeuse);
        assertEquals(OrientationEnum.N, tondeuse.getOrientation());
    }

    @Test
    void rotationDroiteTest() {
        Tondeuse tondeuse = new Tondeuse(new Coordonnees(5,3), OrientationEnum.N);

        tondeuseService.rotationDroite(tondeuse);
        assertEquals(OrientationEnum.E, tondeuse.getOrientation());

        tondeuseService.rotationDroite(tondeuse);
        assertEquals(OrientationEnum.S, tondeuse.getOrientation());

        tondeuseService.rotationDroite(tondeuse);
        assertEquals(OrientationEnum.W, tondeuse.getOrientation());

        tondeuseService.rotationDroite(tondeuse);
        assertEquals(OrientationEnum.N, tondeuse.getOrientation());
    }
}